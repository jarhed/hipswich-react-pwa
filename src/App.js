import React, { Component } from 'react';
import ComposedMap from './components/ComposedMap';
import MainMenu from './components/MainMenu';
import Modal from './components/Modal';
import CameraPane from './components/CameraPane';
import { AppProvider, AppConsumer } from './context/app-context';
import './App.css';

class App extends Component {
  render() {
    return (
      <AppProvider>
        <AppConsumer>
        { ({state, actions}) => (
          <div className='container'>
            { (state.activeLocation) ? (
              <Modal activeLocation={state.activeLocation}></Modal>
            ) : null}
            <ComposedMap></ComposedMap>
            <CameraPane></CameraPane>
            <MainMenu></MainMenu>
          </div>
        )}
        </AppConsumer>
      </AppProvider>
    );
  }
}

export default App;