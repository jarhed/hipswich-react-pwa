import React, { Component } from 'react';
import firebase from 'firebase';
require('firebase/firestore');
const uuid = require('uuid/v4');

const config = {
  apiKey: "AIzaSyDOQjmfMTZasS7V4R3wPnPTbeGtXjxIdS0",
  authDomain: "hipswich-1337.firebaseapp.com",
  databaseURL: "https://hipswich-1337.firebaseio.com",
  projectId: "hipswich-1337",
  storageBucket: "hipswich-1337.appspot.com",
  messagingSenderId: "1004965395503"
};

firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

export const AppContext = React.createContext();
export const AppConsumer = AppContext.Consumer;

export class AppProvider extends Component {

	constructor (props) {
		super(props);
		this.state = {
			uid: null,
			mapVisible: true,
			cameraVisible: false,
			menuVisible: true,
			menuPosition: 'br',
			currentLocation: {
    		lat: 0,
    		lng: 0
			},
			widgets: [],
			progressMessage: null,
			activeLocation: null
		};

		this.videoStream = null;
		this.videoElement = null;
		this.selfieCamera = true;

		this.getCurrentLocation = this.getCurrentLocation.bind(this);
		this.takePhoto = this.takePhoto.bind(this);
		this.toggleCamera = this.toggleCamera.bind(this);
		this.switchCamera = this.switchCamera.bind(this);
		this.attachMap = this.attachMap.bind(this);
		this.toggleModal = this.toggleModal.bind(this);

		this.initFirebase();

	}

	initFirebase () {
		firebase.auth().onAuthStateChanged(user => {
			if (!user) {
				firebase.auth().signInAnonymously()
					.then(anon => {
						this.setState({
							uid: anon.uid
						});
					})
					.catch(err => console.log('Sign in error', err));
			} else {
				this.setState({
					uid: user.uid
				});
			}
		});

		const locationsRef = firebase.firestore().collection('locations');

		locationsRef.onSnapshot((snapShot) => {
			snapShot.docChanges.forEach(item => {
				if (item.type === 'added') {
					this.setState((prevState, props) => ({ 
						widgets: [
							...prevState.widgets,
							{
								key: item.doc.id,
								loc: {
									lat: item.doc.data().location.latitude,
									lng: item.doc.data().location.longitude
								}
							}
						] 
					}));
				}
			});
		})
	}

	getCurrentLocation () {
		navigator.geolocation.getCurrentPosition(
			(location) => {
				this.setState({
					currentLocation: {
						lat: location.coords.latitude,
						lng: location.coords.longitude
					}
				});
				let newCoords = new this.maps.LatLng(location.coords.latitude,location.coords.longitude);
				this.map.panTo(newCoords);
				this.map.setZoom(19);
			}, 
			(err) => console.log(err), { maximumAge: 0 });
	}

	toggleCamera () {

		this.setState(prevState => ({ 
			cameraVisible: !prevState.cameraVisible,
			menuVisible: !prevState.menuVisible,
			mapVisible: !prevState.mapVisible 
		}));

		if (!this.videoStream) {
			this.videoElement = document.querySelector('video');
			this.getStream();
		} else {
			this.stopStream();
		}
	}

	stopStream () {
		if (this.videoStream) {
			this.videoStream.getTracks().forEach(track => track.stop());
			this.videoStream = null;
		}
	}

	getStream () {
		this.stopStream();
		let constraints = {
			video: {
				facingMode: this.selfieCamera ? 'user' : 'environment'
			}
		};

		navigator.mediaDevices.getUserMedia(constraints)
			.then(stream => {
				this.videoStream = stream;
				this.videoElement.srcObject = this.videoStream;
				this.videoElement.onloadedmetadata = () => {
					this.videoElement.play();
				}
			})
			.catch(err => console.log('Error', err));

	}

	takePhoto () {
		this.setState({
			progressMessage: 'Uploading...'
		});
		let canvas = document.createElement('canvas');
		let video = document.querySelector('video');
		canvas.width = video.videoWidth;
		canvas.height = video.videoHeight;
		canvas.getContext('2d').drawImage(video, 0, 0);
		firebase.firestore().collection('locations').add({
			location: new firebase.firestore.GeoPoint(this.state.currentLocation.lat, this.state.currentLocation.lng)
		})
		.then(ref => {
			let storageRef = firebase.storage().ref().child(`images/${uuid()}.png`);
			let uploadRef = storageRef.putString(canvas.toDataURL('image/png'), 'data_url');
			uploadRef.on('state_changed', (snapShot) => {
				this.setState({
					progressMessage: `Uploading...${Math.ceil(snapShot.bytesTransferred / snapShot.totalBytes) * 100}%`
				});
			}, (err) => console.log('Upload error', err), () => {
				this.setState({
					progressMessage: 'Saving location...'
				});
				firebase.firestore().collection('locations').doc(ref.id).collection('images').add({
					imageURL: uploadRef.snapshot.downloadURL
				})
				.then(success => {
					this.toggleCamera();
					this.setState({
						progressMessage: null
					});
				})
				.catch(err => console.log(err));
			})
		});
	}

	switchCamera () {
		this.selfieCamera = !this.selfieCamera;
		this.getStream();
	}

	attachMap (map, maps) {
		this.map = map;
		this.maps = maps;
	}

	toggleModal (location = null) {
		// if (location) {
		// 	let activeWidget = this.state.widgets.filter(item => item.key === location);
		// 	console.log(activeWidget)
		// 	let newCoords = new this.maps.LatLng(activeWidget[0].loc.lat,activeWidget[0].loc.lng);
		// 	this.map.panTo(newCoords);
		// }
		this.setState({
			menuVisible: location ? true : false
		});
		this.setState({
			activeLocation: location
		});
	}

	render () {
		return (
			<AppContext.Provider value={{ 
				state: this.state,
				actions: {
					getCurrentLocation: this.getCurrentLocation,
					takePhoto: this.takePhoto,
					toggleCamera: this.toggleCamera,
					switchCamera: this.switchCamera,
					attachMap: this.attachMap,
					toggleModal: this.toggleModal
				}}}>
				{this.props.children}
			</AppContext.Provider>
		);
	}

}